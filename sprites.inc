#ifndef __CHESS_SPRITES_INC__
#define __CHESS_SPRITES_INC__

#include "dragdrop.hh"

sf::Texture texture;
texture.loadFromFile("textures/board.png");

sf::Sprite board(texture);
sf::Texture wp, wr, wkn, wb, wki_, wqu_;
sf::Texture bp, br, bkn, bb, bki_, bqu_;

wp.loadFromFile("textures/white/pawn.png");
wr.loadFromFile("textures/white/rook.png");
wkn.loadFromFile("textures/white/knight.png");
wb.loadFromFile("textures/white/bishop.png");
wki_.loadFromFile("textures/white/king.png");
wqu_.loadFromFile("textures/white/queen.png");
bp.loadFromFile("textures/black/pawn.png");
br.loadFromFile("textures/black/rook.png");
bkn.loadFromFile("textures/black/knight.png");
bb.loadFromFile("textures/black/bishop.png");
bki_.loadFromFile("textures/black/king.png");
bqu_.loadFromFile("textures/black/queen.png");

Piece wp1(window, wp), wp2(window, wp), wp3(window, wp), wp4(window, wp), wp5(window, wp), wp6(window, wp), wp7(window, wp), wp8(window, wp);
Piece wr1(window, wr), wr2(window, wr);
Piece wkn1(window, wkn), wkn2(window, wkn);
Piece wb1(window, wb), wb2(window, wb);
Piece wki(window, wki_), wqu(window, wqu_);

Piece bp1(window, bp), bp2(window, bp), bp3(window, bp), bp4(window, bp), bp5(window, bp), bp6(window, bp), bp7(window, bp), bp8(window, bp);
Piece br1(window, br), br2(window, br);
Piece bkn1(window, bkn), bkn2(window, bkn);
Piece bb1(window, bb), bb2(window, bb);
Piece bki(window, bki_), bqu(window, bqu_);

std::vector<Piece> sp;

sp.push_back(br1);
sp.push_back(bkn1);
sp.push_back(bb1);
sp.push_back(bqu);
sp.push_back(bki);
sp.push_back(bb2);
sp.push_back(bkn2);
sp.push_back(br2);

sp.push_back(bp1);
sp.push_back(bp2);
sp.push_back(bp3);
sp.push_back(bp4);
sp.push_back(bp5);
sp.push_back(bp6);
sp.push_back(bp7);
sp.push_back(bp8);

sp.push_back(wp1);
sp.push_back(wp2);
sp.push_back(wp3);
sp.push_back(wp4);
sp.push_back(wp5);
sp.push_back(wp6);
sp.push_back(wp7);
sp.push_back(wp8);

sp.push_back(wr1);
sp.push_back(wkn1);
sp.push_back(wb1);
sp.push_back(wqu);
sp.push_back(wki);
sp.push_back(wb2);
sp.push_back(wkn2);
sp.push_back(wr2);

#endif
