CXX := g++
CXXFLAGS := -std=c++17 -g
LDFLAGS := -lsfml-window -lsfml-graphics -lsfml-system

OBJS := main.o

chess: $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^ 

clean:
	rm -f chess $(OBJS)
