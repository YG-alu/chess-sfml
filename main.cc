#include <SFML/Graphics.hpp>

#include <vector>
#include <iostream>

#include "utils.hh"

int main(int argc, char *argv[])
{
    sf::RenderWindow window(sf::VideoMode(800, 800), "Chess");

#include "sprites.inc"


    for (int i = 0; i < sp.size(); i++) {
	      sp[i].setOrigin(sf::Vector2f(sp[i].GetTexture()->getSize().x/2, sp[i].GetTexture()->getSize().y/2));
	      sp[i].setScale(sf::Vector2f(0.6f, 0.6f));
	 	    sp[i].setPosition(sf::Vector2f(100*(i % 8) + 50, 
	 				                (i < 8 ? 50:(i < 16 ? 150:(i < 24 ? 650:750)))));
	  }

	  sf::Vector2i offset(28, 28);
	  sf::Vector2f old, _new;

    bool _move = false;
    float dx, dy;
    int iter;

    std::string figPos;
    
    while (window.isOpen()) {
        sf::Event event;
		    sf::Vector2i position = sf::Mouse::getPosition(window) - offset;

        while (window.pollEvent(event)) {
            switch (event.type) {
            case sf::Event::Closed:
                window.close();
                break;
            case sf::Event::MouseButtonPressed:
              	if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
              	  	for (int i = 0; i < sp.size(); i++) {
              		  	  if (sp[i].GetSprite().getGlobalBounds().contains(position.x, position.y)) {
                            _move = true;
                            old = sp[i].getPosition();
                            dx = position.x - old.x;
                            dy = position.y - old.y;
                            iter = i;
            			      }
            		    }
            	  }
                break;
            case sf::Event::MouseButtonReleased:
                if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                    _move = false;
                    sf::Vector2f tmp = sp[iter].getPosition() + sf::Vector2f(100, 100);
                    _new = sf::Vector2f(100*int(tmp.x/100), 100*int(tmp.y/100));
					          figPos = chessNotes(old) + chessNotes(_new);
					          move(sp, figPos);
					          if (old != _new) str_position += figPos + " ";
					          sp[iter].setPosition(_new);
					          std::cout << str_position << std::endl;
                }
            }
        }

        for (unsigned int i = 0; i < sp.size(); i++) sp[i].move(_move);
        window.clear();
        window.draw(board);    
        for (unsigned int i = 0; i < sp.size(); i++) window.draw(sp[i]); 
        window.draw(sp[iter]);
        window.display();
    }

    return 0;
}
