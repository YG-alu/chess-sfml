#ifndef __DRAG_DROP_CHESS__
#define __DRAG_DROP_CHESS__


#include <SFML/Graphics.hpp>

// From:
// https://stackoverflow.com/questions/65644289/dragging-a-sprite-in-sfml?newreg=21c965ccc8204d809f4707ced5c11b93

class Piece: public sf::Drawable, public sf::Transformable
{
private:
    sf::RenderWindow& window;
    sf::Texture texture;
    sf::Sprite sprite;
    bool moving;
public:
    Piece(sf::RenderWindow& windowRef, const std::string& fileName): window(windowRef)
    {
        if (!texture.loadFromFile(fileName)) exit(1);
        this->sprite.setTexture(this->texture);
        this->moving = false;
        //this->sprite.setScale(128/this->sprite.getGlobalBounds().width,128/this->sprite.getGlobalBounds().height);
    }

    Piece(sf::RenderWindow& windowRef, const sf::Texture& _texture): window(windowRef), texture(_texture)
    {
        this->sprite.setTexture(this->texture);
        this->moving = false;
        //this->sprite.setScale(128/this->sprite.getGlobalBounds().width,128/this->sprite.getGlobalBounds().height);
    }

    Piece(Piece&& rval): window(rval.window)
    {
        this->texture = std::move(rval.texture);
        this->sprite.setTexture(this->texture);
        //this->sprite.setScale(rval.sprite.getScale());
        this->moving = std::move(rval.moving);
    }
    Piece(const Piece& lval): window(lval.window)
    {
        this->texture = lval.texture;
        this->sprite.setTexture(this->texture);
        this->sprite.setScale(lval.sprite.getScale());
        this->moving = lval.moving;
    }

    void draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        states.transform *= this->getTransform();
        target.draw(this->sprite,states);
    }

    void move(bool& movingAPiece)
    {
        sf::Vector2f mousePosition = window.mapPixelToCoords(sf::Mouse::getPosition(window));
        if (this->moving) {
            //this->setPosition(mousePosition.x - this->sprite.getGlobalBounds().width/2,mousePosition.y - this->sprite.getGlobalBounds().height/2);
            this->setPosition(mousePosition.x - 0.3, mousePosition.y - 0.3);
            if (!sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
                this->moving = false;
                movingAPiece = false;
            }
        } else {
            if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && mousePosition.x > this->getPosition().x &&
               mousePosition.y > this->getPosition().y && mousePosition.x < this->getPosition().x + this->sprite.getGlobalBounds().width &&
               mousePosition.y < this->getPosition().y + this->sprite.getGlobalBounds().height && !movingAPiece) {
                this->moving = true;
                movingAPiece = true;
            }
        }
    }

    inline sf::Texture *GetTexture()
    {
        return &this->texture;
    }

    inline sf::Sprite GetSprite()
    {
        return this->sprite;
    }
};


#endif
