#include "dragdrop.hh"


std::string str_position;


sf::Vector2f coord(char a, char b)
{
  int x = int(a) - 97;
  int y = 7 - int(b) + 49;
  return sf::Vector2f(100*x, 100*y);
}

std::string chessNotes(sf::Vector2f position)
{
  std::string ret = "";
  ret += char(position.x/100 + 97);
  ret += char(7 - position.y/100 + 49);
  return ret;
}

void move(std::vector<Piece> vec, std::string str)
{
  sf::Vector2f old = coord(str[0], str[1]);
  sf::Vector2f _new = coord(str[2], str[3]);

  for (int i = 0; i < vec.size(); i++) {
    if (vec[i].GetSprite().getPosition() == _new) vec[i].setPosition(-100, -100);
    if (vec[i].GetSprite().getPosition() == old) vec[i].setPosition(_new);
  }

  if (str == "e1g1") if (str_position.find("e1") == -1) move(vec, "h1f1");
  if (str == "e8g8") if (str_position.find("e8") == -1) move(vec, "h8f8");
  if (str == "e1c1") if (str_position.find("e1") == -1) move(vec, "a1d1");
  if (str == "e8c8") if (str_position.find("e8") == -1) move(vec, "a8d8");
}
